﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32.SafeHandles;
using PBDASHBOARD.Models;
using PBDASHBOARD.Models.ModelMapping;
using PBDASHBOARD.User;

namespace PBDASHBOARD.Data
{
    public class ApplicationDbInitializer

    {  
        public static void Initialize(ApplicationDbContext db, bool isDevelopment,UserManager<CurrentReGroupMember> um)
        {
            
          
            if (!isDevelopment)
            { 
                var admin = new CurrentReGroupMember()
                {
                    Role = UserRole.SuperUser, UserName = "SuperUser",Email = "Blala@uia.no"
                };
                db.Database.Migrate();
                um.CreateAsync(admin, "SuperAdmin1.");
                um.AddToRoleAsync(admin, UserRole.SuperUser);
                
               
                 db.SaveChangesAsync();
                Console.WriteLine(" Migrate ...........");
            } 
            /*db.Database.EnsureDeleted();
            db.Database.EnsureCreated();*/ 
            
          


        }
    }
}

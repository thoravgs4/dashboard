﻿namespace PBDASHBOARD.User
{
    public static class UserRole
    {
        public const string SuperUser = "SuperUser";
        public const string Admin = "Admin";
        public const string User = "Member";
    }
    
} 